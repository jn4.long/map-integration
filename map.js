//instantiate leaflet map
var map = L.map('mapContainer').setView([51.505, -0.09], 13);

var layer  = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="https://carto.com/attributions">CARTO</a>'
}).addTo(map)

//markers markup
var LeafIcon = L.Icon.extend({
    options: {
        shadowUrl: 'img/leaf-shadow.png',
        iconSize: [38, 95],
        shadowSize: [50, 64],
        iconAnchor: [22, 94],
        popupAnchor: [-3, -76]
    }
})

var greenIcon = new LeafIcon({iconUrl: 'img/leaf-green.png'}),
    redIcon = new LeafIcon({iconUrl: 'img/leaf-red.png'})
    orangeIcon = new LeafIcon({iconUrl: 'img/leaf-orange.png'});

//add markers to map
L.icon = function (options) {
    return new L.Icon(options);
};

L.marker([51.5, -0.09], {icon: greenIcon}).addTo(map).bindPopup("I am a green leaf.");
L.marker([51.495, -0.083], {icon: redIcon}).addTo(map).bindPopup("I am a red leaf.");
L.marker([51.49, -0.1], {icon: orangeIcon}).addTo(map).bindPopup("I am an orange leaf.");